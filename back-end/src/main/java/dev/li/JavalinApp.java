package dev.li;

import dev.li.controller.AuthController;
import dev.li.controller.StudentController;
import dev.li.utils.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.post;


public class JavalinApp {

    StudentController studentController = new StudentController();
    AuthController authController = new AuthController();
    SecurityUtil securityUtil = new SecurityUtil();

    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(()->{
        path("students", ()->{
            before("/",authController::authorizeToken);
            get(studentController::handleGetStudentsRequest);
            post(studentController::handlePostNewStudent);
            path(":id",()->{
                before("/", authController::authorizeToken);
                get(studentController::handleGetStudentBySidRequest);
                delete(studentController::handleDeleteById);
            });
        });
        path("login",()-> {
            post(authController::authenticateLogin);
            after("/", securityUtil::attachResponseHeaders);
        });
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }


}
