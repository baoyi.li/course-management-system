const token = sessionStorage.getItem("token");

// take this token, and check it against your server to make sure it is a valid token

if(token){
    // if there is a token, we allow users to create a new item and to log out, if not we allow them to log in
    document.getElementById("login-nav-item").hidden = true;
    
    document.getElementById("log-out-nav-item").hidden = false;
}

// associate the logout functionality with clicking on the logout item on the nav bar
document.getElementById("log-out-nav-item").addEventListener("click", logout);

function logout(){
    sessionStorage.removeItem("token");
    document.getElementById("login-nav-item").hidden = false;
    
    document.getElementById("log-out-nav-item").hidden = true;
}
