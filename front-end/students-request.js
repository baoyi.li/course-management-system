//getting student data from our server

const xhr = new XMLHttpRequest();
xhr.open("GET", "http://localhost:7000/students");
//let token = sessionStorage.getItem("jwt");
const authToken = sessionStorage.getItem("token");
if(authToken){
    xhr.setRequestHeader("Authorization", authToken);
}
xhr.setRequestHeader("Authorization", "admin-auth-token");
xhr.onreadystatechange = function(){
    if(xhr.readyState == 4){
        if(xhr.status == 200){
            const students = JSON.parse(xhr.responseText);
            console.log(students);
            renderStudentsInTable(students);
        }else{
            console.log("something went wrong with your request")
        }
     
    }
}

xhr.send();

function renderStudentsInTable(studentList){
    const tableBody = document.getElementById("students-table-body");
    for(let student of studentList){
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<th scope="row">${student.sid}</th><td>${student.name}</td><td>${student.phone}</td><td>${student.email}</td>`;
        tableBody.appendChild(newRow);
    }
}