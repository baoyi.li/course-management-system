# Course Management System

## Project Description

This is a simple web application that allows the teacher to login, add files, enter students' grade in Gradebook and check the student information

## Technologies Used
Back-end
* Java - version 1.8.0_211
* Hibernate - version 5.4.30.Final
* Javalin - version 3.13.3
* Junit - version 4.12

Front-end
* JavaScript
* HTML
* CSS

Deploy
* Azure Virtual Machines
* Azure Blob Storage
* Azure DevOps

## Features

List of features 
* User Login: Use hardcoded email and password to login the system
* CRUD student information
* CRUD student grade
* Navigation bar

To-do list:
* Upload files to server


## Getting Started

1. Download the project from GitLab
> git clone  https://gitlab.com/baoyi.li/course-management-system.git

2. Open the back-end project in IntelliJ IDEA, go to the Driver Class, right click the Run button, and modify your database configuration in environment variable 

3. run your Driver class

4. Use Postman to test different end point
* /login
* /login/students
* /login/students/:id

5. To checkout the user interface, open the home.html in Google Chrome
## Usage

1. Teacher can login the course management system using the login button
2. Teacher can manage the student using the View all the student button
3. Teacher can create, update, cread and delete the student information in student.html


## License

This project uses the following license: <https://gitlab.com/baoyi.li/course-management-system/-/blob/master/LICENSE>.
